
import os
import re
import glob
import hashlib

"""
Simple file-based cache for saving downloaded HTML, inspired by
scrapelib.cache.FileCache.

"""

class FileCache(object):
    """File-based cache for downloaded HTML."""
    _illegal = re.compile(r'[?/:|]+')
    _maxlen = 200

    def __init__(self, directory):
        self.directory = os.path.join(os.getcwd(), directory)
        if not os.path.isdir(self.directory):
            os.makedirs(self.directory)

    def _clean_key(self, key):
        md5 = hashlib.md5(key.encode('utf8')).hexdigest()
        key = self._illegal.sub(',', key)
        return ','.join((key[:self._maxlen], md5))

    def _path_for_key(self, key):
        """Get cache filepath for key."""
        return os.path.join(self.directory, key)

    def set(self, key, content):
        """Set cache entry for key with file contents."""
        key = self._clean_key(key)
        with open(self._path_for_key(key), 'wb') as fh:
            fh.write(content)

    def get(self, key):
        """Get cache entry for key, or return None."""
        key = self._clean_key(key)
        try:
            with open(self._path_for_key(key), 'rb') as fh:
                return fh.read()
        except IOError:
            return None

    def clear(self):
        """Delete all cache entries (that end with md5 hash)."""
        cache_glob = '*,' + ('[0-9a-f]' * 32)
        for fname in glob.glob(os.path.join(self.directory, cache_glob)):
            os.remove(fname)

    def __str__(self):
        return '<FileCache in {0}>'.format(self.directory)

    def __repr__(self):
        return str(self)

