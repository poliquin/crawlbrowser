
import os
import re
import time
import logging
from itertools import product

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

"""
Object for crawling the web with a (headless) browser.

"""

class LoginException(Exception):
    """An error related to the login process."""
    pass
class SearchException(Exception):
    """An error while performing a search on the page."""
    pass

class WebBrowser(object):
    WS = re.compile(r'\s+')

    def __init__(self, phantom=False):
        """Create web browser.

        :param phantom: set to True to use PhantomJS instead of Firefox
        """
        if phantom:
            self._b = webdriver.PhantomJS()
        else:
            self._b = webdriver.Firefox()
        self.history = []

    def get(self, url, reload=False, retry=1, maxtry=3):
        """Navigate to desired url.

        :param reload: set to True to reload if url is current page
        :param retry: how many times url has been tried already
        :param maxtry: maximum times to try the url in event of failure
        """
        if self._b.current_url != url or reload:
            try:
                self._b.get(url)
            except:
                if retry >= maxtry:
                    raise
                time.sleep(30*max(1, retry))
                self.get(url, reload, retry+1, maxtry)
            self.history.append(url)

    def xpath(self, xpath, multiple_results=False):
        """Find elements using xpath, optionally return multiple matches."""
        try:
            if multiple_results:
                elem = self._b.find_elements_by_xpath(xpath)
            else:
                elem = self._b.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return None
        except:
            raise
        return elem

    def wait_until_title_contains(self, title, timeout=30):
        """Wait until page title contains given string."""
        try:
            _ = WebDriverWait(self._b, timeout).until(EC.title_contains(title))
        except:
            logging.warning('Timeout waiting for title: {0}'.format(title))

    def wait_until_dom_contains_elem(self, elem_id, timeout=30):
        """Wait until page contains element with given id attribute value."""
        try:
            _ = WebDriverWait(self._b, timeout).until(
                    EC.presence_of_element_located((By.ID, elem_id))
                )
        except:
            logging.warning('Timeout waiting for elem id: {0}'.format(title))

    def wait_until_xpath_invisible(self, xpath, timeout=30):
        """Wait until element with given xpath is invisble."""
        try:
            WebDriverWait(self._b, timeout).until(
                EC.invisibility_of_element_located((By.XPATH, xpath))
            )
        except:
            logging.warning('Timeout waiting for invisible: {0}'.format(xpath))

    def wait_until_xpath_visible(self, xpath, timeout=30):
        """Wait until element with given xpath is visble."""
        try:
            WebDriverWait(self._b, timeout).until(
                EC.visibility_of_element_located((By.XPATH, xpath))
            )
        except:
            logging.warning('Timeout waiting for visible: {0}'.format(xpath))

    def wait_for_xpath(self, xpath, timeout=30):
        """Wait for an xpath expression."""
        try:
            _ = WebDriverWait(self._b, timeout).until(
                    EC.presence_of_element_located((By.XPATH, xpath))
                )
        except:
            logging.warning('Timeout waiting for xpath: {0}'.format(xpath))

    def text(self, xpath, multiple_results=False, clean=True):
        """Get text for element(s) identified by xpath expression.

        :param xpath: xpath expression identifying element(s) on page
        :multiple_results: set to True to find multiple elements on page
            matching the xpath expression
        :clean: set to True to remove excess whitespace from text content
        """
        txt = self.xpath(xpath, multiple_results)
        if multiple_results:
            txt = [i.text or '' for i in txt]
        else:
            txt = txt.text or ''
        if not clean:
            return txt
        return self._clean_text(txt)

    def enter_text(self, elem, text, wait=0.05):
        """Enter text into an element that accepts input.

        :param elem: element to receive text input
        :param text: the text to enter (type into the input)
        :param wait: delay between keystrokes
        """
        elem.clear()
        for c in text:
            elem.send_keys(c)
            time.sleep(wait)

    def login(self, username, password):
        """Login to website."""
        # find the inputs for entering username and password
        try:
            usr = self.xpath('//input[contains(@name, "user")]')
            self.enter_text(usr, username)
        except:
            raise LoginException('Could not enter username for login.')
        try:
            pwd = self.xpath('//input[contains(@name, "pass")]')
            self.enter_text(pwd, password)
        except:
            raise LoginException('Could not enter password for login.')
        # find the submit button
        try:
            submit = self.xpath('//input[@type="submit"]')
            submit.click()
        except:
            raise LoginException('Could not find and click sign in button')

    def _look_for_elem(self, tags, attributes, keywords):
        """Search for an element with given tag type(s) that contains a
           one of the given keywords in one of the given attributes.
        """
        for t, a, k in product(tags, attributes, keywords):
            try:
                e = self.xpath('//{0}[contains(@{1}, "{2}")]'.format(t, a, k))
                return e
            except:
                continue
        return None

    def search(self, query, attributes=None, keywords=None):
        """Find a search box and execute a query.

        :param query: search string
        :param attributes: html element attributes that search over while
            looking for search input
        :param keywords: terms to look for in html element attributes while
            looking for search input
        """
        if attributes is None:
            attributes = ['name', 'id', 'class', 'role']
        if keywords is None:
            keywords = ['search', 'query']
        search = self._look_for_elem(['input'], attributes, keywords)
        button = self._look_for_elem(['button'], attributes, keywords)
        # if we haven't found a search input, grab the first test input on page
        if search is None:
            try:
                search = self.xpath('//input[@type="text"]')
            except:
                raise SearchException('Could not find search <input>')
        # if we haven't found search button, find a submit input
        if button is None:
            try:
                button = self.xpath('//input[@type="submit"]')
            except:
                raise SearchException('Could not find search button')
        # execute the search
        self.enter_text(search, query)
        button.click()

    def iter_links(self):
        """Get tuples of link text and href attributes."""
        for a in self._b.find_elements_by_tag_name('a'):
            href = a.get_attribute('href') or ''
            yield (self._clean_text(a.text), href)

    def get_dropdown_options(self, select_xpath):
        """Get option labels and values from a dropdown menu."""
        select = self.xpath(select_xpath)
        if select is None:
            raise Exception('Could not find element: {0}'.format(select_xpath))
        options = select.find_elements_by_tag_name('option')
        for i in options:
            yield (i.get_attribute('value'), i.text.strip(), i)

    def select_value_from_dropdown(self, select_xpath, val):
        """Pick option from <select> dropdown using the value attribute."""
        val = str(val)
        for v, lbl, e in self.get_dropdown_options(select_xpath):
            if v == val:
                e.click()
                break
        else:
            raise Exception('Could not find option with value {0}'.format(val))


    def _scroll_to(self, x=0, y=0, wait=0):
        """Scroll to x, y position, then wait given number of seconds."""
        self._b.execute_script("window.scrollTo({0}, {1});".format(x, y))
        time.sleep(wait)

    def scroll_to_bottom(self, wait=0):
        """Scroll to bottom of the page."""
        self._scroll_to("0", "document.body.scrollHeight", wait)

    def scroll_to_top(self, wait=0):
        """Scroll to top of the page."""
        self._scroll_to("0", "0", wait)

    def _clean_text(self, text):
        """Remove whitespace from string or iterable of strings."""
        if text is None:
            return ''
        if not isinstance(text, str) and not isinstance(text, unicode):
            return [self.WS.sub(' ', i.strip()) for i in text]
        return self.WS.sub(' ', text.strip())

    def __str__(self):
        return '<WebBrowser {0}>'.format(self._b.current_url)

    def __del__(self):
        self._b.close()
        del self.history

    def __getattr__(self, name):
        """Expose attributes of the underlying webdriver instance."""
        return self._b.__getattribute__(name)

