
import re
import lxml.html


class ParseException(Exception):
    pass


def clean_ws(s):
    """Remove excess whitespace from strings."""
    try:
        return re.sub("\s+", " ", s.strip())
    except:
        return s


def build_dom(html, domain=None):
    """Turn html string into lxml.html document object."""
    if domain is not None:
        dom = lxml.html.fromstring(html, base_url=domain)
        dom.make_links_absolute(domain)
    else:
        dom = lxml.html.fromstring(html)
    return dom


def elem_or_default(root, xpath, default=None):
    """Find first matching element or return default."""
    elem = root.xpath(xpath)
    if len(elem) == 0:
        return default
    return elem[0]


def text_or_default(root, xpath, default=''):
    """Find text of first matching element or return default."""
    elem = elem_or_default(root, xpath)
    if elem is None:
        return default
    return clean_ws(elem.text_content())


def class_text_or_default(root, cls, default='', strip=True):
    """Find text of elements with class name or return default."""
    elems = root.find_class(cls)
    texts = [i.text_content() for i in elems]
    if strip:
        # remove empty elements from the results
        texts = [i for i in texts if i is not None]
    else:
        # replace empty elements with default string
        texts = [i or default for i in texts]
    return [clean_ws(i) for i in texts]


def attr_or_default(root, xpath, attribute, default=''):
    """Find attribute of first matching element or return default."""
    elem = elem_or_default(root, xpath)
    if elem is None:
        return default
    attr = elem.get(attribute)
    return attr or default


def parse_table(table):
    """Parse a table into a list of lists containing rows and cells."""
    data = []
    for row in table.iterdescendants('tr'):
        cells = row.iterdescendants(['th', 'td'])
        texts = [clean_ws(c.text_content()) for c in cells]
        data.append(texts)
    return data


def dedup(seq):
    """Remove duplicates from iterable while preserving order."""
    seen = set()
    sadd = seen.add
    return [i for i in seq if not (i in seen or sadd(i))]


# SCRAPING FUNCTIONS
def random_user_agent():
    """Get a random user-agent string."""
    import random
    user_agents = [("Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.11 "
                      "(KHTML, like Gecko) Chrome/23.0.1271.26 Safari/537.11"),
                   ("Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 "
                      "(KHTML, like Gecko) Chrome/31.0.1650.16 Safari/537.36"),
                   ("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) "
                      "Gecko/20100101 Firefox/21.0"),
                   ("Mozilla/5.0 (X11; Linux i686; rv:21.0) Gecko/20100101 "
                      "Firefox/21.0"),
                   ("Mozilla/5.0 (Windows NT 6.2; rv:21.0) Gecko/20130326 "
                      "Firefox/21.0"),
                   ("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) "
                      "Gecko/20130401 Firefox/21.0"),
                   ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) "
                      "AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/39.0.2171.95 Safari/537.36")]
    return random.choice(user_agents)


def build_http_header(url, user_agent=None):
    """Create a dictionary representing a header for an HTTP request."""
    header = {'Accept-Language': 'en-US,en;q=0.8,es;q=0.6'}
    if user_agent is not None:
        header['User-Agent'] = user_agent
    return header

