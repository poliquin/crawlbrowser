Browser for Crawling, Built with Selenium
=========================================

This project is about making it easier to use selenium for crawling/scraping
websites. It's essentially boilerplate code that takes care of:

1. Retrying failed requests
2. Logging into websites with username/password
3. Finding search fields and performing searches
3. Keeping a history of visited pages
4. Iterating through all links
5. Getting text content and cleaning whitespace
6. Waiting for elements on the page

